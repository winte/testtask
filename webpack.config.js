const webpack = require('webpack');
const { resolve } = require('path');
const autoprefixer = require('autoprefixer');
const csso = require('postcss-csso');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const resolver = (condition, a, b) => (condition ? a : b);
const removeEmpty = (items) => items.filter((item) => !!item);

module.exports = ({ production, development }) => {
  const ifProd = resolver.bind(null, production);
  const ifDev = resolver.bind(null, development);

  return {
    context: resolve(__dirname, 'src'),
    entry: {
      bundle: ['babel-polyfill', './index.js'],
    },
    devtool: 'eval-source-map',
    output: {
      path: resolve(__dirname, 'dist'),
      filename: '[name].js',
    },
    plugins: removeEmpty([
      ifDev(new HtmlWebpackPlugin({
        template: './index.html',
        chunks: ['bundle'],
      })),
      ifDev(new webpack.NamedModulesPlugin()),
      ifProd(new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production'),
      })),
      ifProd(new webpack.optimize.UglifyJsPlugin({
        compress: {
          screw_ie8: true,
          warnings: false,
          unused: false,
        },
      })),
    ]),
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          exclude: /node_modules(?![\\/]vue-awesome[\\/])/,
          options: {
            postcss: removeEmpty([
              ifProd(autoprefixer()),
              ifProd(csso()),
            ]),
          },
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader',
        },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            mimetype: 'application/octet-stream',
            limit: '10000',
          },
        },
        {
          test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            prefix: 'font/',
            limit: '5000',
          },
        },
        {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            mimetype: 'image/svg+xml',
            limit: '10000',
          },
        },
        {
          test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'url-loader',
          query: {
            mimetype: 'image/png',
            limit: '1000',
          },
        },
      ],
    },
  };
};
