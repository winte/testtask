import Vue from 'vue';
import VueRouter from 'vue-router';
import Departments from './components/Departments/Departments.vue';
import Employees from './components/Employees/Employees.vue';
import Main from './components/Main.vue';

Vue.use(VueRouter);

export default new VueRouter({
  // mode: 'history',
  routes: [
    {
      name: 'main',
      path: '/',
      component: Main,
    },
    {
      name: 'departments',
      path: '/departments',
      component: Departments,
    },
    {
      name: 'employees',
      path: '/employees',
      component: Employees,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
