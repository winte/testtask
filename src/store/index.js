import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import 'vue-awesome/icons';
import departments from './modules/departments';
import employees from './modules/employees';

Vue.use(Vuex);

export default new Store({
  modules: {
    departments,
    employees,
  },
});
