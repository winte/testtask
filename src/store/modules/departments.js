import { getDepartments } from '../api';
import {
  FETCH_DEPARTMENTS,
  FETCH_DEPARTMENTS_SUCCESS,
  FETCH_DEPARTMENTS_FAILURE,
} from '../mutationTypes';

export default {
  state: {
    departments: [],
    loading: false,
    error: false,
    statusError: 0,
  },
  mutations: {
    [FETCH_DEPARTMENTS](state) {
      state.loading = true;
      state.error = false;
    },
    [FETCH_DEPARTMENTS_SUCCESS](state, departments) {
      state.loading = false;
      state.error = false;
      state.departments = departments;
    },
    [FETCH_DEPARTMENTS_FAILURE](state, { status }) {
      state.loading = false;
      state.error = true;
      state.departments = [];
      state.statusError = status;
    },
  },
  actions: {
    async fetchDepartments({ commit, state }) {
      if (state.loading) return;

      commit(FETCH_DEPARTMENTS);
      try {
        const departments = await getDepartments();
        commit(FETCH_DEPARTMENTS_SUCCESS, departments.data);
      } catch (error) {
        console.error(error);
        commit(FETCH_DEPARTMENTS_FAILURE, error);
      }
    },
  },
};
