import * as api from '../api';
import * as types from '../mutationTypes';

export default {
  state: {
    employees: [],
    loading: false,
    error: false,
    statusError: 0,
  },
  mutations: {
    [types.FETCH_EMPLOYEES](state) {
      state.loading = true;
      state.error = false;
    },
    [types.FETCH_EMPLOYEES_SUCCESS](state, departments) {
      state.loading = false;
      state.error = false;
      state.employees = departments;
    },
    [types.FETCH_EMPLOYEES_FAILURE](state, { status }) {
      state.loading = false;
      state.error = true;
      state.employees = [];
      state.statusError = status;
    },
    [types.DELETE_EMPLOYEES](state) {
      state.error = false;
      state.loading = true;
    },
    [types.DELETE_EMPLOYEES_SUCCESS](state, id) {
      state.error = false;
      state.loading = false;
      state.employees = state.employees.filter(((item) => +item.id !== +id));
    },
    [types.DELETE_EMPLOYEES_FAILURE](state) {
      state.error = true;
      state.loading = false;
    },
    [types.ADD_EMPLOYEES](state) {
      state.error = false;
      state.loading = true;
    },
    [types.ADD_EMPLOYEES_SUCCESS](state, params) {
      state.error = false;
      state.loading = false;
      state.employees = [...state.employees, params];
      console.log(state.employees);
    },
    [types.ADD_EMPLOYEES_FAILURE](state) {
      state.error = true;
      state.loading = false;
    },
  },
  actions: {
    async fetchEmployees({ commit, state }) {
      if (state.loading) return;

      commit(types.FETCH_EMPLOYEES);
      try {
        const employees = await api.getEmployees();
        commit(types.FETCH_EMPLOYEES_SUCCESS, employees.data);
      } catch (error) {
        console.error(error);
        commit(types.FETCH_EMPLOYEES_FAILURE, error);
      }
    },
    async deleteEmployees({ commit, state }, id) {
      if (state.loading) return;

      commit(types.DELETE_EMPLOYEES);
      try {
        await api.deleteEmployees(id);
        commit(types.DELETE_EMPLOYEES_SUCCESS, id);
      } catch (error) {
        console.error(error);
        commit(types.DELETE_EMPLOYEES_FAILURE, error);
      }
    },
    async addEmployees({ commit, state }, params) {
      if (state.loading) return;
      commit(types.ADD_EMPLOYEES);
      try {
        await api.addEmployees(params);
        commit(types.ADD_EMPLOYEES_SUCCESS, params);
      } catch (error) {
        console.error(error);
        commit(types.ADD_EMPLOYEES_FAILURE, error);
      }
    },
  },
};
