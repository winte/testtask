export const FETCH_DEPARTMENTS = 'FETCH_DEPARTMENTS';
export const FETCH_DEPARTMENTS_SUCCESS = 'FETCH_DEPARTMENTS_SUCCESS';
export const FETCH_DEPARTMENTS_FAILURE = 'FETCH_DEPARTMENTS_FAILURE';

export const DELETE_DEPARTMENTS = 'DELETE_DEPARTMENTS';
export const DELETE_DEPARTMENTS_SUCCESS = 'DELETE_DEPARTMENTS_SUCCESS';
export const DELETE_DEPARTMENTS_FAILURE = 'DELETE_DEPARTMENTS_FAILURE';

export const ADD_DEPARTMENTS = 'ADD_DEPARTMENTS';
export const ADD_DEPARTMENTS_SUCCESS = 'ADD_DEPARTMENTS_SUCCESS';
export const ADD_DEPARTMENTS_FAILURE = 'ADD_DEPARTMENTS_FAILURE';

export const FETCH_EMPLOYEES = 'FETCH_EMPLOYEES';
export const FETCH_EMPLOYEES_SUCCESS = 'FETCH_EMPLOYEES_SUCCESS';
export const FETCH_EMPLOYEES_FAILURE = 'FETCH_EMPLOYEES_FAILURE';

export const DELETE_EMPLOYEES = 'DELETE_EMPLOYEES';
export const DELETE_EMPLOYEES_SUCCESS = 'DELETE_EMPLOYEES_SUCCESS';
export const DELETE_EMPLOYEES_FAILURE = 'DELETE_EMPLOYEES_FAILURE';

export const ADD_EMPLOYEES = 'ADD_EMPLOYEES';
export const ADD_EMPLOYEES_SUCCESS = 'ADD_EMPLOYEES_SUCCESS';
export const ADD_EMPLOYEES_FAILURE = 'ADD_EMPLOYEES_FAILURE';
