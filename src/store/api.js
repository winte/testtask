import Axios from 'axios';

const host = 'http://localhost:3001';

export const getDepartments = () => Axios(`${host}/Departments`);
export const deleteDepartments = (id) => {
  if (!id) throw new Error('id is not defined');

  return Axios({
    method: 'delete',
    url: `${host}/Departments/${id}`,
  });
};
export const addDepartments = ({ id, name }) => {
  if (!id) throw new Error('params.id is not defined');
  if (!name) throw new Error('params.name is not defined');

  return Axios({
    method: 'post',
    url: `${host}/Departments/${id}`,
  });
};

export const getEmployees = () => Axios(`${host}/Employees`);
export const deleteEmployees = (id) => {
  if (!id) throw new Error('id is not defined');

  return Axios({
    method: 'delete',
    url: `${host}/Employees/${id}`,
  });
};
export const addEmployees = (data) => {
  if (!data.id) throw new Error('params.id is not defined');
  if (!data.departmentId) throw new Error('params.departmentId is not defined');
  if (!data.firstName) throw new Error('params.firstName is not defined');
  if (!data.lastName) throw new Error('params.lastName is not defined');

  return Axios({
    method: 'post',
    url: `${host}/Employees`,
    data,
  });
};
