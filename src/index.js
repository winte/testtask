import Vue from 'vue';
import router from './router';
import store from './store';
import App from './components/App.vue';

const el = document.getElementById('root');

export default new Vue({
  el,
  router,
  store,
  render: (h) => h(App),
});
